module.exports = {
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
    ],
    "parser": "@typescript-eslint/parser",
    "plugins": ["react", '@typescript-eslint'],
    "parserOptions": {
        "ecmaVersion": 2020,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "env": {
        "browser": true,
        "es2020": true
    },
    "rules": {
        "react/react-in-jsx-scope": "off",
    }
}
