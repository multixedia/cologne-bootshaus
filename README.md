# bootshaus-cologne

# Tech usage:
- [x] [React with Vite](https://vitejs.dev/)
- [x] [TailwindCSS](https://tailwindcss.com/)
- [x] [Typescript](https://www.typescriptlang.org/)
- [x] [ESLint](https://eslint.org/)
- [x] [Icons](https://heroicons.dev/)
- [x] [Material UI with Tailwind](https://www.material-tailwind.com) 
- [x] [Vitest](https://vitest.dev/) and [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/) for testing

# How to run:
- `npm install`
- `npm run dev`

# How to run Unit-Test:
- `npm run test`
