import { CalendarIcon, ListBulletIcon } from '@heroicons/react/24/solid';
import { Squares2X2Icon } from "@heroicons/react/24/outline";

interface IconButtonProps {
    icon?: 'calendar' | 'listBullet' | 'squares2X2';
    onClicked?: () => void;
    isActive?: boolean;
}

export default function IconButton({ icon, onClicked, isActive }: IconButtonProps) {
    let IconTag;

    switch (icon) {
        case 'calendar':
            IconTag = CalendarIcon;
            break;
        case 'listBullet':
            IconTag = ListBulletIcon;
            break;
        case 'squares2X2':
            IconTag = Squares2X2Icon;
            break;
        default:
            IconTag = null;
            break;
    }

    return (
        <button
            className={`${isActive ? 'bg-primary hover:bg-primary' : 'bg-white'} p-2 rounded-lg hover:bg-gray-200 transition-colors duration-500`}
            data-testid="IconButton"
            onClick={onClicked}
        >
            {IconTag && <IconTag className={`${isActive ? 'text-white' : 'text-primary'} w-6 h-6`} />}
        </button>
    );
}
