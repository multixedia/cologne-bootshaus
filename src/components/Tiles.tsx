import React from "react";

interface TilesProps {
    children: React.ReactNode;
}

const Tiles: React.FC<TilesProps> = ({ children }) => {
    return <ul data-testid="tiles">{children}</ul>;
};

export default Tiles;
