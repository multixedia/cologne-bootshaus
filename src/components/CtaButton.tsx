import { Button } from "@material-tailwind/react";
import React from "react";
import { ChevronRightIcon } from '@heroicons/react/24/solid'

interface CtaButtonProps {
    label: string;
    onClicked: () => void;
    children?: React.ReactNode;
    arrow?: boolean;
    disabled?: boolean;
    size?: 'sm' | 'lg';
}

export default function CtaButton(props: CtaButtonProps) {
    const {
        arrow = true,
        disabled = false,
        label,
        onClicked,
        children,
        size
    } = props;

    const ctaTheme = `
        group px-8 lg:px-16 border inline-flex items-center relative normal-case font-normal text-[18px]
        ${disabled ? 'disabled:opacity-30' : ''} 
        hover:border-primary hover:bg-white hover:text-primary hover:shadow-none`
    ;

    return (
        <>
            <Button disabled={disabled} className={ctaTheme} size={size} onClick={onClicked} data-label={label}>
                {children}
                {arrow ? (
                    <ChevronRightIcon
                        className={`h-6 w-6 text-white group-hover:text-primary absolute right-2 
                            ${size === 'sm' ? 'top-[10px]' : 'top-[16px]'}
                        `}
                    />
                ) : null}
            </Button>
        </>
    );
}
