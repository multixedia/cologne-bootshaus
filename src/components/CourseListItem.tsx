import { MapPinIcon } from "@heroicons/react/24/outline";
import { formatDate, formatTime } from "~/helper/dateConverter.ts";
import { useTranslation } from "react-i18next";
import { formatPrice } from "~/helper/priceFormat.ts";
import CtaButton from "~/components/CtaButton.tsx";

interface ContainerProps {
    title: string;
    startDate: string;
    price: number;
    location?: string;
    isSoldOut?: boolean;
}

export default function CourseListItem(props: ContainerProps) {
    const { title, startDate, price, location, isSoldOut } = props;
    const { t } = useTranslation();

    const ctaClicked = () => {
        console.log('Button List clicked!');
    };

    return (
        <>
            <li className={'flex flex-col md:flex-row odd:bg-gray-200 even:bg-white lg:p-6 p-3'} data-testid="CourseListItem">
                <div className={'flex flex-col md:w-8/12 xl:w-9/12 md:flex-row'}>
                    <div className={'flex flex-col sm:w-10/12 md:w-8/12 lg:w-9/12 xl:w-10/12 xl:flex-row'}>
                        <div className={'xl:w-9/12 lg:pr-6 pb-3'}>
                            <a href="/" className={'font-bold'}>
                                {title}
                            </a>
                            <div className={'flex pt-2'}>
                                <MapPinIcon className={'h-5'} /> <span className={'inline-block pl-2'}>{location === 'Köln' ? `Bootshaus, ${location}` : location}</span>
                            </div>
                        </div>
                        <div className={'xl:w-3/12'}>
                            <div>{t('dateBegin')} <span className={''}>{formatDate(startDate)}</span></div>
                            <div>{t('timeBegin')} <span className={''}>{formatTime(startDate, t('language'))}</span></div>
                        </div>
                    </div>
                    <div className={'md:pl-6 py-3 md:pt-0 md:w-4/12 lg:w-3/12 xl:w-2/12'}>
                        {t('ticket.buyTicketsFrom')}
                        <div className={'inline-block md:block font-bold'}>{formatPrice(price).fullWord}</div>
                    </div>
                </div>
                <div className={'md:w-4/12 xl:w-3/12 flex md:justify-end items-start'}>
                    <CtaButton size={'sm'} label="Click me!" disabled={isSoldOut} onClicked={ctaClicked}>
                        {t('ticket.label')}
                    </CtaButton>
                </div>
            </li>
        </>
    )
}
