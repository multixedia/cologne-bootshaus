import { MagnifyingGlassIcon } from "@heroicons/react/24/outline";
import { ChangeEvent, useState } from 'react';

interface SearchProps {
    // eslint-disable-next-line no-unused-vars
    onSearchTermChange: (searchTerm: string) => void;
}

function Search({ onSearchTermChange }: SearchProps) {
    const [searchTerm, setSearchTerm] = useState('');

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newSearchTerm = event.target.value;
        setSearchTerm(newSearchTerm);
        onSearchTermChange(newSearchTerm); // Call the callback function to notify the parent component
    };

    return (
        <div className="w-full sm:max-w-[298px] relative">
            <MagnifyingGlassIcon className={'absolute top-[10px] w-6 left-2'} />
            <input
                className="w-full bg-gray-200 border-none p-2 pt-3 pl-10 rounded-md"
                type="search"
                value={searchTerm}
                onChange={handleInputChange}
                placeholder=""
                data-testid="search-input"
            />
        </div>
    );
}

export default Search;
