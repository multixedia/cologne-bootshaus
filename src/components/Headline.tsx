import React from 'react';

interface HeadlineProps {
    tag: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
    className?: string;
    children: React.ReactNode;
}

export default function Headline({ tag, className, children }: HeadlineProps) {
    const HeadlineTag = tag;

    return <HeadlineTag className={className}>{children}</HeadlineTag>;
}
