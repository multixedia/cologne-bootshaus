import React from "react";

type Props = {
    children?: React.ReactNode;
};

export default function Container(props: Props) {
    const { children } = props;

    return (
        <div className="container mx-auto p-3" data-testid="Container">
            {children}
        </div>
    );
}
