import CtaButton from "~/components/CtaButton.tsx";
import { useTranslation } from "react-i18next";
import { formatDate, formatTime } from "~/helper/dateConverter.ts";
import { formatPrice } from "~/helper/priceFormat.ts";
import { ClockIcon, CalendarDaysIcon, MapPinIcon, TicketIcon } from '@heroicons/react/24/outline'

interface ContainerProps {
    imageUrl: string;
    title: string;
    startDate: string;
    endDate: string;
    location: string;
    price: number;
    availableTickets: number | null;
    isSoldOut?: boolean;
}

export default function Tile(props: ContainerProps) {
    const { imageUrl, title, startDate, location, price, isSoldOut, availableTickets } = props;

    const { t } = useTranslation();
    const ctaClicked = () => {
        console.log('Button tile clicked!');
    };

    return (
        <li className="flex xl:justify-between items-start gap-4 py-4 flex-col px-0 lg:px-0 md:flex-row">
            <div className="flex items-start flex-col lg:flex-row">
                <img className="max-w-[527px] rounded-lg mb-4 w-full" src={imageUrl} alt={title} />
            </div>
            <div className={'flex flex-col lg:flex-row items-start justify-start gap-6 w-full lg:justify-between'}>
                <div className="xl:pl-5 lg:w-6/12">
                    <h2 className="h2 mb-3 lg:mb-6 text-primary">{title}</h2>

                    {startDate && (
                        <div className={'pb-2 flex'}>
                            <CalendarDaysIcon className={'h-5'} /> <span className={'inline-block pr-6 pl-2'}>{formatDate(startDate)}</span>
                            <ClockIcon className={'h-5'} /> <span className={'pl-2 pr-1'}>{formatTime(startDate, t('language'))}</span>
                        </div>
                    )}

                    {location && (
                        <div className={'pb-2 flex'}>
                            <MapPinIcon className={'h-5'} /> <span className={'inline-block pl-2'}>{location === 'Köln' ? `Bootshaus, ${location}` : location}</span>
                        </div>
                    )}

                    {price > 0 && (
                        <div className={'pb-2 flex'}>
                            <TicketIcon className={'h-5'} /> <span className={'inline-block pl-2'}>{t('ticket.buyTicketsFrom')} {formatPrice(price).euros}</span>
                        </div>
                    )}

                    {availableTickets && (
                        <div className={'text-orange-500 font-bold'}>
                            {t('ticket.availableTickets').replace('{quantity}', String(availableTickets))}
                        </div>
                    )}
                </div>
                <div className={'lg:w-6/12 lg:flex lg:justify-end'}>
                    <CtaButton label="Click me!" disabled={isSoldOut} onClicked={ctaClicked}>
                        {t('ticket.label')}
                    </CtaButton>
                </div>
            </div>
        </li>
    );
}
