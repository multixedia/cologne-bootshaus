import React from "react";

interface propsValue {
    children?: React.ReactNode;
}

export default function CourseList(props: propsValue) {

    const { children } = props;

    return (
        <>
            <ul data-testid="CourseList">
                {children}
            </ul>
        </>
    )
}
