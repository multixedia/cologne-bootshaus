function Hero () {

    return (
        <div
            className="flex justify-center py-6 lg:py-12 xl:py-18 max-w-[520px] mx-auto"
            data-testid="Hero"
        >
            <img
                alt="logo"
                className=""
                src="/images/logo.jpg"
            />
        </div>
    );

}

export default Hero;
