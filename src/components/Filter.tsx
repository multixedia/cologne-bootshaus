import React, { useState } from 'react';
import Search from '~/components/form/Search.tsx';

interface ContainerProps {
    children?: React.ReactNode;
    // eslint-disable-next-line no-unused-vars
    onSearchTermChange: (searchTerm: string) => void;
}

export default function Filter(props: ContainerProps) {
    const [, setSearchTerm] = useState('');

    const handleSearchTermChange = (newSearchTerm: string) => {
        setSearchTerm(newSearchTerm);
        props.onSearchTermChange(newSearchTerm); // Returning the search term to the parent
    };

    const { children } = props;

    return (
        <>
            <div className="flex flex-col md:flex-row gap-4 lg:w-1/2 justify-end">
                <Search onSearchTermChange={handleSearchTermChange} />
                {children}
            </div>
        </>
    );
}
