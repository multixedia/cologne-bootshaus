import i18next from "i18next";
import {initReactI18next} from "react-i18next";
import translations from "./assets/translations.json";

const { VITE_DEFAULT_LANGUAGE } = import.meta.env;

i18next.
    use(initReactI18next).
    init({
        "debug": false,
        "resources": translations,
        "lng": VITE_DEFAULT_LANGUAGE, // Set the default language here
        "interpolation": {
            "escapeValue": false
        }
    }).
    then(() => {

        console.log("i18n initialized");

    });

export default i18next;
