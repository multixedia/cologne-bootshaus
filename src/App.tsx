import { useTranslation } from 'react-i18next';
import { useEffect, useState } from 'react';
import fetchData, { Item } from './helper/fetchData';
import Hero from './components/Hero';
import Container from './components/Container';
import Filter from './components/Filter';
import Headline from "~/components/Headline";
import IconButton from '~/components/IconButton';
import Tiles from '~/components/Tiles';
import Tile from '~/components/Tile';
import CourseList from "~/components/CourseList";
import CourseListItem from "~/components/CourseListItem";

function App() {
    const { t } = useTranslation();
    const [originalData, setOriginalData] = useState<Item[]>([]);
    const [data, setData] = useState<Item[]>([]);
    const [activeIcon, setActiveIcon] = useState('squares2X2');


    useEffect(() => {
        const getData = async () => {
            const fetchedData = await fetchData('/api-data.json');
            setOriginalData(fetchedData);
            setData(fetchedData);
        };

        getData().then(() => console.info('Data fetched!'));
    }, []);

    if (!data) {
        return <div>Loading...</div>;
    }

    const handleIconClick = (iconName: string) => {
        setActiveIcon(iconName);
    };
    // SEARCH
    const handleSearchTermChange = (newSearchTerm: string) => {
        if (newSearchTerm.trim() === "") {
            setData(originalData);
        } else {
            const filteredData = originalData.filter((item) =>
                item.title.toLowerCase().includes(newSearchTerm.toLowerCase())
            );
            setData(filteredData);
        }
    };
  return (
    <>
      <div className="w-full">
          <Hero />
          <Container>
              <div className="flex flex-col sm:flex-row justify-between w-full pb-4 lg:pb-8">
                  <Headline tag="h1" className="h1 lg:w-1/2 flex items-end text-primary">{t('ticket.title')}</Headline>
                  <Filter onSearchTermChange={handleSearchTermChange}>
                      <div className="flex items-center gap-2">
                          <span className={'translate-y-1 inline-block pr-1'}>{t('view')}:</span>
                          <IconButton
                              icon="squares2X2"
                              onClicked={() => handleIconClick('squares2X2')}
                              isActive={activeIcon === 'squares2X2'}
                          />
                          <IconButton
                              icon="listBullet"
                              onClicked={() => handleIconClick('listBullet')}
                              isActive={activeIcon === 'listBullet'}
                          />
                          <IconButton
                              icon="calendar"
                              onClicked={() => handleIconClick('calendar')}
                              isActive={activeIcon === 'calendar'}
                          />
                      </div>
                  </Filter>
              </div>

              { activeIcon === 'squares2X2' &&
                  <Tiles>
                      {data.map((item, index) => (
                          <Tile
                              key={`tile-${index}`}
                              imageUrl={item.imageUrl}
                              title={item.title}
                              startDate={item.startDate}
                              endDate={item.endDate}
                              location={item.address.addressLocality}
                              price={item.priceFrom}
                              isSoldOut={item.priceFrom === 0}
                              availableTickets={item.availableTickets}
                          />
                      ))}
                  </Tiles>
              }

              { activeIcon === 'listBullet' &&
                  <CourseList>
                      {data.map((item, index) => (
                          <CourseListItem
                              key={`list-${index}`}
                              title={item.title}
                              startDate={item.startDate}
                              price={item.priceFrom}
                              location={item.address.addressLocality}
                              isSoldOut={item.priceFrom === 0}
                          />
                      ))}
                  </CourseList>
              }

              { activeIcon === 'calendar' &&
                  <div>Calendar view</div>
              }
          </Container>
      </div>
    </>
  )
}

export default App;
