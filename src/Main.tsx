import React from 'react';
import ReactDOM from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import i18n from './i18n'; // Import the i18next configuration
import App from './App.tsx';
import './assets/styles/index.css';
import { ThemeProvider } from "@material-tailwind/react";
import themeButton from "./theme/button.ts";

const theme = {
    ...themeButton,
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
      <I18nextProvider i18n={i18n}>
          <ThemeProvider value={theme}>
              <App />
          </ThemeProvider>
      </I18nextProvider>
  </React.StrictMode>
)

