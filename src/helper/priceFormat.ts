export function formatPrice(price: number) {
    const priceString = parseFloat(String(price)).toLocaleString('de-DE', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });

    return {
        euros: `${priceString} €`,
        fullWord: `${priceString} Euro`
    };
}
