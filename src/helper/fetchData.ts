export interface Address {
    "@type": string;
    streetAddress: string;
    addressLocality: string;
    addressRegion: string | null;
    postalCode: string;
    addressCountry: string;
}

export interface Item {
    title: string;
    startDate: string;
    endDate: string;
    imageUrl: string;
    shopUrl: string;
    address: Address;
    priceFrom: number;
    isSoldOut: boolean;
    availableTickets: number | null;
}

async function fetchData(path: string): Promise<Item[]> {
    try {
        const response = await fetch(path);
        return await response.json();
    } catch (error) {
        console.error('Error fetching JSON:', error);
        return [];
    }
}

export default fetchData;
