export function formatDate(dateString: string) {
    const date = new Date(dateString);
    const options = { weekday: 'short', day: '2-digit', month: '2-digit', year: 'numeric' } as const;
    return date.toLocaleDateString('de-DE', options);
}

export function formatTime(dateString: string, language: string) {
    const date = new Date(dateString);
    const options = { hour: '2-digit', minute: '2-digit' } as const;
    const timeString = date.toLocaleTimeString(language, options);
    const period = language === 'de-DE' ? 'Uhr' : '';
    return `${timeString} ${period}`;
}


