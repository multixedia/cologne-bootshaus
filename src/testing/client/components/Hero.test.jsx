import { describe, test, expect } from 'vitest'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect';
import Hero from '@/components/Hero'


describe("<Hero />", () => {
    test('renders logo image', () => {
        render(<Hero />);
        const logoImage = screen.getByAltText('logo');
        expect(logoImage).toBeInTheDocument();
        expect(logoImage).toHaveAttribute('src', '/images/logo.jpg');
    });

    test('renders Hero component with correct data-testid', () => {
        render(<Hero />);
        const heroComponent = screen.getByTestId('Hero');
        expect(heroComponent).toBeInTheDocument();
    });
})
