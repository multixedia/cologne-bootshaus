import { describe, test, expect, vi } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import Filter from '@/components/Filter';

// Testing Mock Functions
describe('Filter', () => {
    test('calls onSearchTermChange with the new search term', () => {
        const onSearchTermChangeMock = vi.fn();
        const searchTerm = 'example';

        const { getByTestId } = render(
            <Filter onSearchTermChange={onSearchTermChangeMock} />
        );

        const searchInput = getByTestId('search-input');

        fireEvent.change(searchInput, { target: { value: searchTerm } });

        expect(onSearchTermChangeMock).toHaveBeenCalledWith(searchTerm);
    });
});
