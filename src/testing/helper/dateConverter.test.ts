import {describe, expect, it} from "vitest";
import {formatDate, formatTime} from "~/helper/dateConverter.ts";

describe(
    "formatDate",
    () => {

        it(
            "should format date correctly",
            () => {

                const dateString = "2023-05-05T23:00:00+02:00",
                    formattedDate = formatDate(dateString);
                expect(formattedDate).toEqual("Fr., 05.05.2023");

            }
        );

    }
);

describe("formatTime", () => {
        it("should format time DE correctly", () => {
            const dateString = "2023-05-05T23:00:00+02:00",
                formattedTime = formatTime(
                    dateString,
                    "de-DE"
                );
            expect(formattedTime).toEqual("23:00 Uhr");
            }
        );

        it("should format time EN correctly with PM", () => {
                const dateString = "2023-05-05T23:00:00+02:00",
                    formattedTime = formatTime(
                        dateString,
                        "en-EN"
                    ),
                    // Remove non-breaking space character from the received value
                    receivedTime = formattedTime.replace(
                        /\u202f/g,
                        " "
                    ).trim();
                expect(receivedTime).toBe("11:00 PM");
            }
        );

        it("should format time EN correctly with AM", () => {
                const dateString = "2023-05-05T09:30:00+02:00",
                    formattedTime = formatTime(
                        dateString,
                        "en-EN"
                    ),
                    // Remove non-breaking space character from the received value
                    receivedTime = formattedTime.replace(
                        /\u202f/g,
                        " "
                    ).trim();
                expect(receivedTime).toBe("09:30 AM");
            }
        );

    }
);
