import { describe, it, expect } from "vitest";
import { formatPrice } from '@/helper/priceFormat'

describe('formatPrice', () => {
    it('should format price correctly', () => {
        const price = 1234.5678;
        const expectedEuros = '1.234,57 €';
        const expectedFullWord = '1.234,57 Euro';
        const result = formatPrice(price);

        // Assert
        expect(result.euros).toEqual(expectedEuros);
        expect(result.fullWord).toEqual(expectedFullWord);
    });
});
