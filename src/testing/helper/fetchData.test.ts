import { test, assert } from 'vitest';
import fetchData, { Item } from '@/helper/fetchData';

test('fetchData - success', async () => {
    const mockData: Item[] = [
        {
            "title": "Tiefblaue Nacht @ Bootshaus Blckbx",
            "startDate": "2023-05-05T23:00:00+02:00",
            "endDate": "2023-05-06T05:30:00+02:00",
            "imageUrl": "https://cdn.ticket.io/companies/DMnDlIN6/events/utcy3t1u/img/holder-1080.jpg?74238ecb",
            "shopUrl": "https://bootshaus-club.ticket.io/utcy3t1u/",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Auenweg 173",
                "addressLocality": "Köln",
                "addressRegion": null,
                "postalCode": "51063",
                "addressCountry": "Deutschland"
            },
            "isSoldOut": false,
            "priceFrom": 15,
            "availableTickets": 24
        }
    ];

    globalThis.fetch = () =>
        Promise.resolve({
            json: () => Promise.resolve(mockData),
        }) as unknown as Promise<Response>;

    const path = '/api-data.json';
    const result = await fetchData(path);

    assert.deepEqual(result, mockData);
});

test('fetchData - error', async () => {
    const errorMessage = 'Error fetching JSON';
    let loggedErrorMessage = '';

    // Mock the console.error method to capture the error message
    console.error = (message: string) => {
        loggedErrorMessage = message;
    };

    globalThis.fetch = () =>
        Promise.reject(new Error(errorMessage)) as unknown as Promise<Response>;

    const path = '/api-data.json';
    const result = await fetchData(path);

    assert.deepEqual(result, []);
    assert.equal(loggedErrorMessage, `${errorMessage}:`);
});
