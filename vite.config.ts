import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path';
import eslintPlugin from 'vite-plugin-eslint'
import dotenv from 'dotenv';

// Load environment variables from .env file
dotenv.config();
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
      react(),
      eslintPlugin({
        cache: false,
        include: ['./src/**/*.js', './src/**/*.jsx'],
        exclude: [],
    })
  ],
  // @ts-ignore
  test: {
    globals: true,
    environment: 'happy-dom'
  },
  resolve: {
    alias: {
      // eslint-disable-next-line no-undef
      '@': path.resolve(__dirname, 'src'),
      // eslint-disable-next-line no-undef
      '~': path.resolve(__dirname, 'src'),
    },
  },
})
